let trainer = {

	name: 'Ash Ketchum',
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoen: ['May', 'Max'],
		kanto: ['Brock','Misty']
	}
	
}
console.log(trainer)
console.log("Result of dot Notation")
console.log(trainer.name)
console.log("Result of square bracket Notation")
console.log(trainer['pokemon'])
console.log("Result of talk method")
let trainerTalk = {
	name: 'Pikachu',
	talk: function(){
		console.log(this.name + '!' + ' I choose you!'  )
	}
}
trainerTalk.talk()


function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;



	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name)

		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
			target.health = (target.health - this.attack)
			if(target.health <= 0){
				target.name.faint();
			}

	};
	
	this.faint = function(){
		console.log(this.name + ' fainted')
	}
	
}
let pikachu = new Pokemon('Pikachu', 12)
let geodude = new Pokemon('Geodude', 8)
let mewtwo = new Pokemon('Mewtwo', 100)


console.log(pikachu)
console.log(geodude)
console.log(mewtwo)

geodude.tackle(pikachu)
console.log(pikachu)

mewtwo.tackle(geodude)
geodude.faint()

console.log(geodude)
let grades = [98.5, 94.3, 89.2, 90.1]

console.log(grades[0])

// an object is similar to an array. It is a collection of related data/or functionionalites.Usually it represent real world objects

let grade = {
	// object initializer/literal notation - Objects consist of properties, which are used to describe an object. Key-Value Pair
	math: 89.2,
	english: 98.5,
	science: 90.1,
	filipino:94.3
}
// we use dot notation to access the /properties value of our object
console.log(grade.english)

// Syntax:
/*
	let objectName = {
		keyA: valueA,
		keyB: valueB
	}

*/

let cellphone = {
	brandName: 'Nokia 3310',
	color: 'Dark Blue',
	manufactureDate:1999
}
console.log(typeof cellphone)


let student ={
	firstName: 'John',
	lastName: 'Smith',
	location: {
		city:'Tokyo',
		country: 'Japan'
	},
	emails: ['john@mail.com', 'johnsmith@mail.xyz'],
	// object method (function inside an object)
	fullName: function(){
		return this.firstName + ' ' + this.lastName
	}
}
// dot notation
console.log(student.location.city)
// bracket notation
console.log(student['firstName'])
console.log(student.emails)
console.log(student.emails[0])
console.log(student.fullName())

// sample for 'this'
const person1 = {
	name: 'Jane',
	greeting: function(){
		return 'hi I\'m' + this.name
	}
}
console.log(person1.greeting())

// ARRAY OF OBJECTS
let contactList = [
	{
		firstName: 'John',
		lastName: 'Smith',
		location: 'Japan'
	},
	{
		firstName: 'Jane',
		lastName: 'Smith',
		location: 'Japan'
	},
	{
		firstName: 'Jasmine',
		lastName: 'Smith',
		location: 'Japan'
	}
]
console.log(contactList[0].firstName)


let people = [
	
	{
		name: 'Juanita',
		age: 13
	},
	{
		name: 'Juanito',
		age: 14
	}

]

people.forEach(function(person){
	console.log(person.name)
})
console.log(`${people[0].name} are the list`)

// Creating objects using a Constructor Function(JS OBJECT CONSTRUCTORS/OBJECT from BLUEPRINTS)

// Creates a reusable function to create several objects that have the same data structure
// This is useful for creating multiple copies/instances of an object
// Object.Literals
// let object = {}
// Instance - it has a distinct or unique objects
// let object = new object
// An instance is a concrete occurence of any object which emphasizes on the unique identity of it
/*
Syntax:
	function ObjectName(keyA, keyB){
		this.keyA = keyA,
		this.keyB = keyB
	}

*/

function Laptop(name, manufactureDate){
	// The 'this' keyword allows to assign a new object's property by associating them with values received from our parameter
	this.name = name,
	this.manufactureDate = manufactureDate
}

// This is a unique instance of the Laptop object
let laptop = new Laptop('Lenovo', 2008)
console.log('Result from creating objects using object constructors')
console.log(laptop)

// This is another unique instance of the Laptop object
let myLaptop = new Laptop('MacBook Air', 2020)
console.log('Result from creating objects using object constructors')
console.log(myLaptop)

let oldLaptop = Laptop('Vaio', 2008)
console.log(oldLaptop)

// Creating empty objects
let computer = {}
let myComputer = new Object();


console.log(myLaptop.name)
console.log(myLaptop['name'])

let array = [laptop, myLaptop]

console.log(array[0].name)

// Initializing/Adding/Deleting/Reassigning OBJECT PROPERTIES

// initialize/add properties after the object was created
// this is useful for times when an object's properties are undetermined at the time of creating them

let car = {}

// add an object properties, we can use dot notation
car.name = 'Honda Civic'
console.log(car)
car['manufacture date'] = 2019;
console.log(car)
// Reassiging object properties
car.name = 'Honda Accord'
console.log(car)
// deleting object properties
delete car.name
console.log('Result from deleting properties')
console.log(car)

// OBJECT METHODS
// A method is a function which is a property of an object
// They are also functions and one of the key differences they have is that methods are functions related to a specific object

let person = {
	name: 'John',
	talk: function(){
		console.log('Hello my name is ' + this.name)
	}
}
console.log(person)
console.log('Result from object methods')
person.talk()

// Adding methods to object person
person.walk = function(){
	console.log(this.name + ' walked 25 steps forward')
}
person.walk()

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address:{
		city: 'Austin',
		country: 'Texas'
	},
	emails: ['joe@email.com', 'joesmith@asd.com'],
	introduce: function(){
		console.log('Hello my name is ' + this.firstName + ' ' + this.lastName)
	}
}
friend.introduce()


/*Real world Application of Objects
-Scenario
	1. We would like to create a game that would have several pokemon interact with each other.
	2. Every pokemon would have the same set of stats, properties and functions.
*/
// Using object literals to create multiple kinds of pokemon would be time consuming
let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log('This pokemon tackled target Pokemon')
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	},
	faint: function(){
		console.log('Pokemon fainted')
	}
}


// Creating an object constructor instead of object literals.

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name)
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_ ")
	};

	this.faint = function(){
		console.log(this.name + 'fainted')
	}
}
let pikachu = new Pokemon('Pikachu', 16)
let charizard = new Pokemon('Charizard', 8)

pikachu.tackle(charizard)

console.log(pikachu)

